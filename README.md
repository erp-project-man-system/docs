# Intro
This repo contains all the documentation related to the project including research, reports, and project plan and technical design document.

# Context
To find more about the context of the project, you can check the Project plan and the Technical design document.

# Architecture
Want to see what architecture style is utilized in the project? Check out Architecture report.

# Scalability and Performance
Check the main research report for more insights on what techniques are used in the project.

# DevSecOps and Cloud
Interested in DevOps and Cloud, then you should check out DevSecOps and Cloud research, as well as Cloud security.

# Data and Security
Want to know how data is designed and what measure are taken in terms of security? Find out in Data and Security research, also check out Cloud security report.

# Conclusion
In the future new docs are planned to be added, I am thinking of levergaing markdown documents as more flexible format in terms of VCS control.